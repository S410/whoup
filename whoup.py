#!/usr/bin/env python3

import os
import sys
import asyncio
from enum import Enum
from subprocess import DEVNULL
from typing import Any, Callable, Optional, Union, Iterable, cast
from types import FrameType
from itertools import chain
import gi
import signal

gi.require_version("Notify", "0.7")
from gi.repository import Notify  # type: ignore

CHECK_PERIOD: float = 30.0  # Seconds
PING_TIMEOUT: int = 2  # Seconds
PING_COUNT: int = 3

XDG_CONFIG_HOME = os.environ.get(
    "XDG_CONFIG_HOME", os.environ.get("HOME", "/tmp") + "/.config"
)

APP_NAME = "WhoUp"
APP_NOTI_ICON = "computer"
CONFIG_FILE = XDG_CONFIG_HOME + "/whoup.conf"


def log(*args: Any, **kwargs: Any) -> None:
    kwargs["file"] = sys.stderr
    print(*args, **kwargs)


class StateChange(Enum):
    UP = 0
    DOWN = 1
    NONE = 2


SignalHandlerFunction = Callable[
    [Union[int, signal.Signals], Optional[FrameType]], None
]


class SignalHandler:
    def __init__(
        self,
        sig: signal.Signals,
        *handlers: SignalHandlerFunction,
    ):
        self._handlers = list(handlers)

        signal.signal(sig, self)

    def add_handler(self, handler: SignalHandlerFunction) -> None:
        self._handlers.append(handler)

    def __call__(
        self, sig: Union[int, signal.Signals], frame: Optional[FrameType]
    ) -> None:
        for h in self._handlers[::-1]:
            h(sig, frame)


class Machine:
    def __init__(self, ip: str, name: str = ""):
        self.ip = ip
        self.name = name

        self.was_up = False

    async def is_up(self) -> bool:
        self.was_up = await ping(self.ip)
        return self.was_up

    async def has_changed_state(self) -> StateChange:
        m = (self.was_up, await self.is_up())
        if m == (True, False):
            return StateChange.DOWN
        elif m == (False, True):
            return StateChange.UP
        else:
            return StateChange.NONE

    def __str__(self) -> str:
        return self.name if self.name != "" else self.ip

    def __repr__(self) -> str:
        return str(self)


class MachineList(list[Machine]):
    def __init__(self, machines: list[Machine]):
        list.__init__(self, machines)

    async def find_changed(self) -> tuple[list[Machine], list[Machine]]:
        """
        find_changed return a tuple with two lists of machines.
        First list is machines that went up, second is machines that went down.

        If no machines have changed state since the last call, two empty
        lists are returned.
        """

        went_up = []
        went_down = []

        for (sc, m) in zip(
            await asyncio.gather(*[m.has_changed_state() for m in self]),
            self,
        ):
            if sc == StateChange.UP:
                went_up.append(m)
            elif sc == StateChange.DOWN:
                went_down.append(m)

        return (went_up, went_down)

    async def who_up(self) -> tuple[list[Machine], list[Machine]]:
        """
        find_changed return a tuple with two lists of machines.
        First list is machines that are up, second is machines that are down.
        """

        up_down: tuple[list[Machine], list[Machine]] = ([], [])

        for (is_up, m) in zip(
            await asyncio.gather(*[m.is_up() for m in self]),
            self,
        ):
            # Machines that are up go first
            #       not True -> False -> 0
            #       not False -> True -> 1
            up_down[not is_up].append(m)

        return up_down

    def who_was_up(self) -> tuple[list[Machine], list[Machine]]:
        """
        find_changed return a tuple with two lists of machines.
        First list is machines that were up the last time they were checked,
        the second list is machines that were down.
        """

        up_down: tuple[list[Machine], list[Machine]] = ([], [])

        for (is_up, m) in zip(
            [m.was_up for m in self],
            self,
        ):
            up_down[not is_up].append(m)

        return up_down


async def ping(ip: str) -> bool:
    subproc = await asyncio.create_subprocess_exec(
        f"ping",
        "-c",
        f"{PING_COUNT}",
        "-W",
        f"{PING_TIMEOUT}",
        f"{ip}",
        stdout=DEVNULL,
        stderr=DEVNULL,
    )

    return await subproc.wait() == 0


def temp_noti(
    body: str = "",
    autoclear: float = 0.0,
) -> Notify.Notification:
    n = Notify.Notification.new(APP_NAME, body, APP_NOTI_ICON)

    if autoclear > 0.0:
        asyncio.get_event_loop().call_later(autoclear, n.close)

    n.show()

    return n


def parse_args(args: list[str]) -> MachineList:
    return MachineList(
        [
            Machine(ip, name)
            for [ip, name, *_] in map(
                lambda x: x.strip().split(" ", 1) + [""], args
            )
        ]
    )


def parse_conf() -> MachineList:
    try:
        with open(CONFIG_FILE) as f:
            return parse_args(list(f))
    except FileNotFoundError:
        log(f"{CONFIG_FILE} not found!")
        return MachineList([])


def print_help() -> None:
    print(f"Usage: {sys.argv[0]} [-d] [ADDR NAME...]")
    print(r"    -d")
    print(r"        run as a daemon. Use dbus to display notifications.")
    print()
    print(f"    If no ADDR NAME arguments are provided")
    print(f"    {CONFIG_FILE} is parsed instead")


def join_human(l: Iterable[Any]) -> str:
    l = list(l)

    if len(l) == 0:
        return ""

    if len(l) == 1:
        return str(l[0])

    return f"{', '.join(map(str, l[:-1]))} and {l[-1]}"


def is_are(l: Iterable[Any], adj: str) -> str:
    l = list(l)

    if len(l) == 0:
        return ""

    return f"{join_human(l)} {'is' if len(l) == 1 else 'are'} {adj}"


def join_truthy(s: Iterable[str], sep: str = "\n") -> str:
    return sep.join(filter(None, s))


def up_down_rows(up: list[Machine], down: list[Machine]) -> str:
    return join_truthy(
        chain((f"{x} is UP" for x in up), (f"{x} is DOWN" for x in down))
    )


def up_down_human(up: list[Machine], down: list[Machine]) -> str:
    return join_truthy([is_are(up, "UP."), is_are(down, "DOWN.")])


async def daemon(machines: MachineList) -> None:
    status = Notify.Notification.new(APP_NAME, "", APP_NOTI_ICON)
    status.set_urgency(Notify.Urgency.LOW)
    
    close = lambda *_: status.close()
    onSIGTERM.add_handler(close)
    onSIGINT.add_handler(close)

    while True:
        (went_up, went_down) = await machines.find_changed()

        (up, _) = machines.who_was_up()
        if up:
            status.update(APP_NAME, join_human(up), APP_NOTI_ICON)
            status.show()
        else:
            status.close()

        if went_up or went_down:
            log(up_down_rows(went_up, went_down))
            temp_noti(up_down_human(went_up, went_down), 5.0)

        await asyncio.sleep(CHECK_PERIOD)


async def main() -> Any:
    args = sys.argv[1:]
    daemonize = False
    if args:
        [f, *a] = args
        if f in ("help", "-h", "--help"):
            print_help()
            return
        elif f == "-d":
            daemonize = True
            args = a

    machines = parse_args(args) or parse_conf()
    if len(machines) == 0:
        return "No machines to monitor!"

    log(up_down_rows(*await machines.who_up()))

    if daemonize:
        await daemon(machines)

    return


if __name__ == "__main__":
    Notify.init(APP_NAME)

    onSIGTERM = SignalHandler(signal.SIGTERM, lambda *_: sys.exit())
    onSIGINT = SignalHandler(signal.SIGINT, lambda *_: sys.exit())

    sys.exit(asyncio.run(main()))
