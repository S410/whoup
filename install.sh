#!/bin/bash

name=whoup

ser="${XDG_CONFIG_HOME:=${HOME}/.config}/systemd/user"
bin="$HOME/.local/bin"
binf="$bin/$name"
serf=("$name"{.service,-restart.{service,path}})

service() {
	systemctl --user "$1" --now "$serf" "${serf[2]}"
}

@install() {
	mkdir -p "$ser" "$bin" 

	cp "${serf[@]}" "$ser"
	sed -i "s/###/${binf//\//\\\/}/g" "$ser/${serf}"
	cp "$name.py" "$binf"; chmod +x "$_"

	service enable
}

@uninstall() {
	service disable
	rm "${serf[@]/#/"$ser/"}" "$binf"
}

_Fs=`declare -F | grep -Po '(?<=.{11}@).*' | paste -sd'|'`

shopt -s extglob
case "$1" in
    @($_Fs))
        "@$1"
        ;;
    *)
        printf '    %s\n' $'\u0d''Commands: ' `tr '|' ' ' <<< "$_Fs"`
        ;;
esac
